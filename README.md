# KlaroCPQ demo project

This repository contains predefined ready-to-use KlaroCPQ project.

## Installing

Clone the repo with it's submodules:

```
git clone --recursive git@gitlab.com:klaro/docker-demo.git
```

Copy the `.env.example` as an `.env` file.

You'll need to pass your id_rsa key into the container to get cloning from gitlab working there:

```bash
export PRIVATE_KEY=`cat "$HOME"/.ssh/id_rsa`
docker-compose build --build-arg PRIVATE_KEY="$PRIVATE_KEY" klaro-demo
```

Another option is to add the key into the container as `/root/.ssh/id_rsa` file manually later.

## Running

On MacOS you'll need `docker-sync` installed and running (with `docker-sync start`) before to get the source files
syncing between the host and container (if needed).

Add `127.0.0.1 liveapps.local` record to your hosts file.

The `docker-compose up` should be enough for running everything if above install instructions were made.

## Cache

To clean symfony cache run:

```bash
docker-compose exec klaro-demo rm -rf /opt/projects/klaro-demo/var/cache
```

## Usage

The project should be accessible in browser with `http://liveapps.local/klaro-demo/` now. 

Log in with `admin` login and `test` password.

Follow doc.klarocpq.com to get further instructions. The `projects/klaro-demo/README.md` file can also be useful. 
